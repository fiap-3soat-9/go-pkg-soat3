package aws

import (
	"context"
	"sync"

	application "gitlab.com/fiap-3soat-9/go-pkg-soat3/starter"
	awstrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/aws/aws-sdk-go-v2/aws"

	pgkConfig "gitlab.com/fiap-3soat-9/go-pkg-soat3/config"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
)

var (
	awsConfig     *aws.Config
	awsConfigOnce sync.Once
)

func GetConfig() *aws.Config {
	awsConfigOnce.Do(func() {
		conf, err := config.LoadDefaultConfig(
			context.TODO(),
			getOptions()...,
		)

		if err != nil {
			panic(err)
		}

		awsConfig = &conf
		awstrace.AppendMiddleware(awsConfig)

	})

	return awsConfig
}

func getOptions() []func(*config.LoadOptions) error {
	var options []func(*config.LoadOptions) error
	conf := application.GetAwsConfig()

	if conf.Region != "" {
		options = append(options, config.WithRegion(conf.Region))
	}

	customResolver := getCustomResolver(conf)
	options = append(options, config.WithEndpointResolverWithOptions(customResolver))

	if conf.Profile != "" {
		options = append(options, config.WithSharedConfigProfile(conf.Profile))
	}

	return options
}

func getCustomResolver(conf pgkConfig.AwsConfig) aws.EndpointResolverWithOptions {
	return aws.EndpointResolverWithOptionsFunc(
		func(service, region string, options ...interface{}) (aws.Endpoint, error) {
			if conf.Endpoint != "" {
				return aws.Endpoint{
					PartitionID:   "aws",
					URL:           conf.Endpoint,
					SigningRegion: conf.Region,
				}, nil
			}

			// returning EndpointNotFoundError will use default resolution
			return aws.Endpoint{}, &aws.EndpointNotFoundError{}
		},
	)
}
