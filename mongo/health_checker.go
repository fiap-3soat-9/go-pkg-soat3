package mongo

import (
	"context"
	"fmt"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/healthcheck"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type healthCheckOptions struct {
	isCritical bool
	name       string
}

type options struct {
	healthCheckOptions healthCheckOptions
}

func HealthCheckIsCritical(isCritical bool) Opt {
	ensureNotInitialized()
	return func(o *options) error {
		o.healthCheckOptions.isCritical = isCritical
		return nil
	}
}

func HealthCheckName(name string) Opt {
	ensureNotInitialized()
	return func(o *options) error {
		o.healthCheckOptions.name = name
		return nil
	}
}

func initHealthChecker() {
	stdHealthChecker = &healthcheck.StandardChecker{
		Name:     opts.healthCheckOptions.name,
		Critical: opts.healthCheckOptions.isCritical,
		Checker:  check,
	}
}

func check(ctx context.Context) []healthcheck.Result {
	result := make([]healthcheck.Result, 0)
	for name, client := range clients {
		err := func(c *mongo.Client) error {
			ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
			defer cancel()
			return c.Ping(ctx, nil)
		}(client)

		if err != nil {
			result = append(result, healthcheck.Result{
				Status:  healthcheck.StatusDown,
				Service: fmt.Sprintf("MongoDB: %s", name),
				Message: err.Error(),
			})
		} else {
			result = append(result, healthcheck.Result{
				Status:  healthcheck.StatusUp,
				Service: fmt.Sprintf("MongoDB: %s", name),
			})
		}
	}

	return result
}

func GetHealthChecker() healthcheck.HealthChecker {
	ensureInitialized()
	return stdHealthChecker
}
