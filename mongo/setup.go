package mongo

import (
	"context"
	"fmt"
	"strings"
	"time"

	"gitlab.com/fiap-3soat-9/go-pkg-soat3/healthcheck"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/starter"
	"go.mongodb.org/mongo-driver/mongo"
	mongoOptions "go.mongodb.org/mongo-driver/mongo/options"
)

type Opt func(*options) error

func Initialize(optionsParam ...Opt) {
	ensureCreated()
	ensureNotInitialized()

	for _, op := range optionsParam {
		err := op(opts)
		if err != nil {
			panic(err)
		}
	}

	initClients()
	initHealthChecker()
	isInitialized = true
}

func initClients() {
	clients = make(map[string]*mongo.Client)
	databaseConfig := starter.GetDatabasesConfig()

	for dbName, dbConf := range databaseConfig {
		uri := fmt.Sprintf("mongodb://%s:%s@%s:%d/%s", dbConf.User, dbConf.Password, dbConf.Host, dbConf.Port, dbName)
		clientOpts := mongoOptions.Client().ApplyURI(uri)

		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		client, err := mongo.Connect(ctx, clientOpts)
		if err != nil {
			panic(fmt.Errorf("failed to connect to %s: %v", dbName, err))
		}
		clients[strings.ToLower(dbName)] = client
	}
}

func ensureCreated() {
	initOnce.Do(func() {
		stdHealthChecker = &healthcheck.StandardChecker{}
		opts = &options{
			healthCheckOptions: healthCheckOptions{
				name:       "Mongo Database",
				isCritical: true,
			},
		}
	})
}

func ensureNotInitialized() {
	if isInitialized {
		panic("mongo database already initialized")
	}
}

func ensureInitialized() {
	if !isInitialized {
		panic("mongo database must be initialized")
	}
}
