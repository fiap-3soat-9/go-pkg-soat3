package mongo

// Config structure assuming what might typically be included in a MongoDB config
type Config struct {
	Host         string
	Port         int
	DatabaseName string
	User         string
	Password     string
}
