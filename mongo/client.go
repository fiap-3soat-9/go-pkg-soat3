package mongo

import (
	"strings"
	"sync"

	"gitlab.com/fiap-3soat-9/go-pkg-soat3/healthcheck"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	clients          map[string]*mongo.Client
	stdHealthChecker *healthcheck.StandardChecker
	opts             *options
	initOnce         sync.Once
	isInitialized    = false
)

func GetClient(name string) *mongo.Client {
	if c, ok := clients[strings.ToLower(name)]; ok {
		return c
	}
	return nil
}
