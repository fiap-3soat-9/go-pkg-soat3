package mongo

import (
	"fmt"
)

// Define the custom error type for missing configuration
type missingRequiredConfigError string

// Implement the Error() method for the missingRequiredConfigError type
func (e missingRequiredConfigError) Error() string {
	return fmt.Sprintf("missing required configuration: %q", string(e))
}

// ValidateConfig checks for essential MongoDB configuration entries
func ValidateConfig(conf Config) error {
	if conf.Host == "" {
		return missingRequiredConfigError("database.host")
	}
	if conf.DatabaseName == "" {
		return missingRequiredConfigError("database.databaseName")
	}
	// Optionally check for user and password if your application requires authentication
	if conf.User == "" {
		return missingRequiredConfigError("database.user")
	}
	if conf.Password == "" {
		return missingRequiredConfigError("database.password")
	}
	return nil
}
