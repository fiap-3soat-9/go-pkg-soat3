package starter

import "gitlab.com/fiap-3soat-9/go-pkg-soat3/config"

// Creates server with default configuration.
func ensureCreated() {
	initOnce.Do(func() {
		opts = &options{}
		appInstance = &app{
			configOptions: defaultConfigOptions(),
			configRoot:    &config.Root{},
		}
	})
}

func ensureNotInitialized() {
	if isInitialized {
		panic("application already initialized")
	}
}

func ensureInitialized() {
	if !isInitialized {
		panic("application must be initialized")
	}
}
