package sqs

import (
	"context"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	awssqs "github.com/aws/aws-sdk-go-v2/service/sqs"
	sqstypes "github.com/aws/aws-sdk-go-v2/service/sqs/types"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/util"
)

type SQSMessageConsumer struct {
	config SQSMessageConsumerConfig
	client *awssqs.Client
	errors chan error
}

func NewSQSMessageConsumer(
	queueUrl string,
	config aws.Config,
) messaging.MessageConsumer {
	cfg := NewConfig(queueUrl, config)
	return NewSQSMessageConsumerFromConfig(cfg)
}

func NewSQSMessageConsumerFromConfig(config SQSMessageConsumerConfig) messaging.MessageConsumer {
	client := awssqs.NewFromConfig(config.awsConfig)
	errors := make(chan error)
	return &SQSMessageConsumer{config, client, errors}
}

func (c *SQSMessageConsumer) Errors() <-chan error {
	return c.errors
}

func (c *SQSMessageConsumer) Run(ctx context.Context, channel chan<- messaging.Message) error {
	defer close(c.errors)

	consume := decorate(c.config.decorator, func(ctx context.Context) error {
		return c.consume(ctx, channel)
	})

	g := util.NewWorkerGroup()
	g.Spawn(c.config.parallelism, func() {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				err := consume(ctx)

				if err != nil {
					c.errors <- err
					<-time.After(c.config.errorBackoff)
					continue
				}
			}
		}
	})
	g.Wait()

	return nil
}

func (c *SQSMessageConsumer) consume(ctx context.Context, channel chan<- messaging.Message) error {
	response, err := c.client.ReceiveMessage(ctx, &awssqs.ReceiveMessageInput{
		QueueUrl:            &c.config.queueUrl,
		MaxNumberOfMessages: int32(c.config.batchSize),
		WaitTimeSeconds:     int32(c.config.batchWaitTime.Truncate(time.Second).Seconds()),
	})

	if err != nil {
		return err
	}

	err = c.config.limiter.
		WaitN(ctx, len(response.Messages))

	for _, message := range response.Messages {
		channel <- &SQSMessage{
			message:  message,
			body:     *message.Body,
			client:   c.client,
			queueUrl: c.config.queueUrl,
		}
	}

	return err
}

type SQSMessage struct {
	message  sqstypes.Message
	body     string
	client   *awssqs.Client
	queueUrl string
}

func (m *SQSMessage) Body() string {
	return m.body
}

func (m *SQSMessage) MarkAsRead(ctx context.Context) (err error) {
	_, err = m.client.DeleteMessage(ctx, &awssqs.DeleteMessageInput{
		QueueUrl:      &m.queueUrl,
		ReceiptHandle: m.message.ReceiptHandle,
	})
	return
}
