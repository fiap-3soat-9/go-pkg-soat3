package sqs

import (
	"context"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"golang.org/x/time/rate"
)

type SQSMessageConsumerFunc func(ctx context.Context) error

type SQSMessageConsumerConfig struct {
	awsConfig     aws.Config
	queueUrl      string
	parallelism   int
	batchSize     int
	limiter       *rate.Limiter
	batchWaitTime time.Duration
	errorBackoff  time.Duration
	decorator     func(ctx context.Context, consumer SQSMessageConsumerFunc) error
}

type SQSMessageConsumerOption func(*SQSMessageConsumerConfig)

func WithRateLimiter(r time.Duration, bucket int) SQSMessageConsumerOption {
	return func(cfg *SQSMessageConsumerConfig) {
		cfg.limiter = rate.NewLimiter(rate.Every(r), bucket)
	}
}

func WithParallelism(parallelism int) SQSMessageConsumerOption {
	return func(cfg *SQSMessageConsumerConfig) {
		cfg.parallelism = parallelism
	}
}

func WithBatchSize(batchSize int) SQSMessageConsumerOption {
	return func(cfg *SQSMessageConsumerConfig) {
		cfg.batchSize = batchSize
	}
}

func WithBatchWaitTime(batchWaitTime time.Duration) SQSMessageConsumerOption {
	return func(cfg *SQSMessageConsumerConfig) {
		cfg.batchWaitTime = batchWaitTime
	}
}

func WithErrorBackoff(errorBackoff time.Duration) SQSMessageConsumerOption {
	return func(cfg *SQSMessageConsumerConfig) {
		cfg.errorBackoff = errorBackoff
	}
}

func WithContextBuilder(builder func(ctx context.Context) context.Context) SQSMessageConsumerOption {
	return WithDecorator(func(ctx context.Context, consumer SQSMessageConsumerFunc) error {
		return consumer(builder(ctx))
	})
}

func WithFinisher(finisher func(ctx context.Context, err error)) SQSMessageConsumerOption {
	return WithDecorator(func(ctx context.Context, consumer SQSMessageConsumerFunc) error {
		err := consumer(ctx)
		finisher(ctx, err)
		return err
	})
}

type decoratorFunc func(ctx context.Context, consumer SQSMessageConsumerFunc) error

func WithDecorator(decorator decoratorFunc) SQSMessageConsumerOption {
	return func(cfg *SQSMessageConsumerConfig) {
		cfg.decorator = composeDecorator(decorator, cfg.decorator)
	}
}

func decorate(decorator decoratorFunc, consumer SQSMessageConsumerFunc) SQSMessageConsumerFunc {
	return func(ctx context.Context) error {
		return decorator(ctx, consumer)
	}
}

func composeDecorator(f, g decoratorFunc) decoratorFunc {
	return func(ctx context.Context, consumer SQSMessageConsumerFunc) error {
		return f(ctx, decorate(g, consumer))
	}
}

const defaultParallelism = 5
const defaultBatchSize = 10
const defaultBatchWaitTime = 20 * time.Second
const defaultErrorBackoff = 50 * time.Millisecond

func NewConfig(queueUrl string, awsConfig aws.Config, opts ...SQSMessageConsumerOption) SQSMessageConsumerConfig {
	cfg := SQSMessageConsumerConfig{
		queueUrl:      queueUrl,
		awsConfig:     awsConfig,
		parallelism:   defaultParallelism,
		batchSize:     defaultBatchSize,
		batchWaitTime: defaultBatchWaitTime,
		errorBackoff:  defaultErrorBackoff,
		limiter:       rate.NewLimiter(rate.Inf, defaultBatchSize),
		decorator:     func(ctx context.Context, consumer SQSMessageConsumerFunc) error { return consumer(ctx) },
	}
	for _, opt := range opts {
		opt(&cfg)
	}

	// the bucket size must be at least batchSize
	if cfg.limiter.Burst() < cfg.batchSize {
		cfg.limiter.SetBurst(cfg.batchSize)
	}

	return cfg
}
