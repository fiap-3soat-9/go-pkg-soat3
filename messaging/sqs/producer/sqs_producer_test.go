package producer

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/starter"
)

type testMessage struct {
	Name string
}

func TestSQSPublisherTestSuite(t *testing.T) {
	suite.Run(t, new(SQSPublisherTestSuite))
}

type SQSPublisherTestSuite struct {
	suite.Suite
	messagePayload testMessage
}

func (suite *SQSPublisherTestSuite) SetupSuite() {
	starter.Initialize()
	suite.messagePayload = testMessage{Name: "test message"}
}

func (suite *SQSPublisherTestSuite) Test_arnFormat() {
	publisher := NewSQSProducer(context.TODO(), "TEST_QUEUE", message.JsonSerializer[testMessage]{Payload: suite.messagePayload})
	expected := "arn:aws:sqs:::TEST_QUEUE"
	suite.T().Log(publisher.Arn)
	assert.Equal(suite.T(), expected, publisher.Arn)
}

func (suite *SQSPublisherTestSuite) Test_bindMessageAttributes() {
	publisher := NewSQSProducer(context.TODO(), "TEST_QUEUE", message.JsonSerializer[testMessage]{Payload: suite.messagePayload}).
		WithMessageAttributes(map[string]struct{ Type, Value string }{
			"event_type": {Type: "String", Value: "CARDS_CREATED"},
		})

	expected := "CARDS_CREATED"
	assert.Equal(suite.T(), &expected, publisher.Attrs["event_type"].StringValue)
}

func (suite *SQSPublisherTestSuite) Test_sqsPublisher_WithEvent() {
	messageTemplate := *message.NewMessageWrapper[testMessage]().
		WithBody(suite.messagePayload).
		WithTopicEvent("EVENT_TEST").
		WithTopic("TEST_QUEUE")

	publisher := NewSQSProducer(context.TODO(), "TEST_QUEUE", message.JsonSerializer[message.Message[testMessage]]{Payload: messageTemplate})
	serializeMessage, err := publisher.Payload.Serialize()
	assert.NotNil(suite.T(), serializeMessage)
	assert.Nil(suite.T(), err)
	assert.NotNil(suite.T(), publisher.Arn)
	assert.NotNil(suite.T(), publisher.Queue)
}
