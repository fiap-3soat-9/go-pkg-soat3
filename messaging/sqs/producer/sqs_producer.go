package producer

import (
	"context"
	"fmt"
	"log"
	"sync"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/starter"

	"github.com/aws/aws-sdk-go-v2/service/sqs/types"
	config "gitlab.com/fiap-3soat-9/go-pkg-soat3/aws"
)

var (
	sqsInstance *sqs.Client
	sqsOnce     sync.Once
)

type SqsProducer struct {
	ctx          context.Context
	messageInput *sqs.SendMessageInput
	Payload      message.Serializer
	Queue        string
	Arn          string
	Attrs        map[string]types.MessageAttributeValue
}

/*
Produce send the event to topic
*/
func (p *SqsProducer) Producer() error {

	serializedMessage, err := p.Payload.Serialize()
	if err != nil {
		log.Println(p.ctx, "[produce Failed] failed to serialize", err)
		return err
	}
	p.messageInput = &sqs.SendMessageInput{
		MessageBody:       serializedMessage,
		MessageAttributes: p.Attrs,
		QueueUrl:          getQueueUrl(p.ctx, p.Queue),
	}

	_, err = sqsClient().SendMessage(p.ctx, p.messageInput)

	if err != nil {
		log.Println(p.ctx, "[Producing Failed]", err)
		return err
	}
	return nil
}

/*
WithMessageAttributes sets attributes to event
returns the instance of SqsPublisher
*/
func (p *SqsProducer) WithMessageAttributes(attrs map[string]struct{ Type, Value string }) *SqsProducer {
	for k, v := range attrs {
		p.Attrs[k] = types.MessageAttributeValue{
			DataType:    aws.String(v.Type),
			StringValue: aws.String(v.Value),
		}
	}
	return p
}

/*
NewSQSPublisher creates a new publisher informing topic name and message with this respective serializer
returns a new instance of SqsPublisher
*/
func NewSQSProducer(ctx context.Context, queue string, msg message.Serializer) *SqsProducer {
	producer := &SqsProducer{
		ctx:     ctx,
		Payload: msg,
		Queue:   queue,
		Arn:     arnFormat(queue),
		Attrs:   make(map[string]types.MessageAttributeValue),
	}

	return producer
}

func arnFormat(sqsQueue string) string {
	arnBase := fmt.Sprintf(
		"arn:aws:sqs:%s:%s",
		starter.GetAwsConfig().Region,
		starter.GetAwsConfig().AccountId,
	)
	return fmt.Sprintf("%s:%s", arnBase, sqsQueue)
}

func getQueueUrl(ctx context.Context, queueName string) *string {
	input := &sqs.GetQueueUrlInput{
		QueueName: &queueName,
	}
	urlResult, err := sqsClient().GetQueueUrl(ctx, input)
	if err != nil {
		log.Println(ctx, "error getting queue url", err)
		return nil
	}
	return urlResult.QueueUrl
}

func sqsClient() *sqs.Client {
	sqsOnce.Do(func() {
		sqsInstance = sqs.NewFromConfig(*config.GetConfig())
	})
	return sqsInstance
}
