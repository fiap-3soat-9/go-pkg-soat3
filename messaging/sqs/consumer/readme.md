`auto generated documentation`

# consumer
--
    import "summer/v0/messaging/sqs/consumer"

Package consumer provides utilities for consuming from sqs queues The consumer
package will load configuration use go-packages/config

    # Configuration

config will searching your configurations, that should will be like example:

    aws:
    region: us-east-1
    accountId: "000000000000"
    endpoint: http://localhost:4566
    accessKeyId: "foo"
    secretAccessKey: "foo"
    profile: ""

## Usage

#### func  NewConsumer

```go
func NewConsumer[T any](ctx context.Context, queueName string, handler MessageHandler[T])
```
NewConsumer registry a consumer to sqs

    # Message

    For be a Message Valid should follow that payload example
    {
    "body": {
    	"topic": "TEST_TOPIC_NAME",
    	"name": "TEST_EVENT",
    	"contents": {
    		"name": "product name"
    		},
    	"version": "2019-07-09",
    	"source": "hamburgueriacard-my-app-name",
    	"createdAt": "2022-08-23T17:41:44.02712-03:00"
    	},
    "createdAt": "2022-08-23T17:41:44.02712-03:00"
    }

#### func  NewConsumerWithErrDecorator

```go
func NewConsumerWithErrDecorator[T any](
	ctx context.Context,
	queueUrl string,
	handler MessageHandler[T],
	errDecorator func(ctx context.Context, errChan <-chan error),
)
```
NewConsumerWithErrDecorator registry a consumer with decorator function option
to sqs

    For be a Message Valid should follow that payload example

{ "body": {

    "topic": "TEST_TOPIC_NAME",
    "name": "TEST_EVENT",
    "contents": {
    	"name": "product name"
    	},
    "version": "2019-07-09",
    "source": "hamburgueriacard-my-app-name",
    "createdAt": "2022-08-23T17:41:44.02712-03:00"
    },

"createdAt": "2022-08-23T17:41:44.02712-03:00" }

#### type Message

```go
type Message[T any] struct {
	BodyString string
}
```

Message represents the received message from SQS (this is not a raw message)

#### func (Message[T]) ReadBody

```go
func (m Message[T]) ReadBody() (T, error)
```
ReadBody deserialize the content string to the given type

#### type MessageHandler

```go
type MessageHandler[T any] func(ctx context.Context, message Message[T]) error
```

MessageHandler defines the handler function that will process the received
message
