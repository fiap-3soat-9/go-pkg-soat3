package consumer

import (
	"context"
	"fmt"
)

func ExampleNewConsumer() {

	type Product struct {
		Name string `json:"name"`
	}

	handler := func(ctx context.Context, message Message[Product]) error {
		product, err := message.ReadBody()
		if err != nil {
			return err
		}

		fmt.Println(product.Name)
		return nil
	}

	ctx := context.TODO()
	NewConsumer(ctx, "hamburgueria-template", handler)
}

func ExampleNewConsumerWithErrDecorator() {

	type Product struct {
		Name string `json:"name"`
	}

	handler := func(ctx context.Context, message Message[Product]) error {
		product, err := message.ReadBody()
		if err != nil {
			return err
		}

		fmt.Println(product.Name)
		return nil
	}

	ErrDecorator := func(ctx context.Context, errChan <-chan error) {
		for err := range errChan {
			fmt.Println(err)
		}
	}

	ctx := context.TODO()
	NewConsumerWithErrDecorator(ctx, "hamburgueria-template", handler, ErrDecorator)
}
