// Package consumer provides utilities for consuming from sqs queues
// The consumer package will load configuration use go-packages/config
/* # Configuration
config will searching your configurations, that should will be like example:
		aws:
		region: us-east-1
		accountId: "000000000000"
		endpoint: http://localhost:4566
		accessKeyId: "foo"
		secretAccessKey: "foo"
		profile: ""
*/
package consumer
