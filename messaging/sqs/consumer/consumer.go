package consumer

import (
	"context"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/aws"
	config "gitlab.com/fiap-3soat-9/go-pkg-soat3/aws"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging"
	sqsConfig "gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/sqs"
)

var (
	sqsInstance *sqs.Client
	sqsOnce     sync.Once
)

// MessageHandler defines the handler function that will process the received message
type MessageHandler[T any] func(ctx context.Context, message Message[T]) error

//NewConsumer registry a consumer to sqs
/*
	# Message

	For be a Message Valid should follow that payload example
	{
	"body": {
		"topic": "TEST_TOPIC_NAME",
		"name": "TEST_EVENT",
		"contents": {
			"name": "product name"
			},
		"version": "2019-07-09",
		"source": "hambuergueria-my-app-name",
		"createdAt": "2022-08-23T17:41:44.02712-03:00"
		},
	"createdAt": "2022-08-23T17:41:44.02712-03:00"
	}
*/
func NewConsumer[T any](ctx context.Context, queueName string, handler MessageHandler[T]) {

	// Find queue url from queue name
	queueUrl := getQueueUrl(ctx, queueName)

	// create new sqs client instance from aws
	cfg := sqsConfig.NewConfig(queueUrl, *aws.GetConfig())

	// create new consumer from hamburgueria messaging lib
	consumer := sqsConfig.NewSQSMessageConsumerFromConfig(cfg)

	// handler consumer errors
	go handlerErrChan(ctx, consumer.Errors())

	// building handler for hamburgueria messaging ling
	msgHandler := buildMessageHandler(handler)

	processor := messaging.NewChannelMessageProcessor(1, msgHandler)
	go handlerErrChan(ctx, processor.Errors())

	log.Println(ctx, fmt.Sprintf("subscribe on queue: %q", queueUrl))

	// run consumer
	go messaging.RunWithSignal(ctx, consumer, processor)
}

// NewConsumerWithErrDecorator registry a consumer with decorator function option to sqs
/* For be a Message Valid should follow that payload example
{
"body": {
	"topic": "TEST_TOPIC_NAME",
	"name": "TEST_EVENT",
	"contents": {
		"name": "product name"
		},
	"version": "2019-07-09",
	"source": "hamburgueria-my-app-name",
	"createdAt": "2022-08-23T17:41:44.02712-03:00"
	},
"createdAt": "2022-08-23T17:41:44.02712-03:00"
}
*/
func NewConsumerWithErrDecorator[T any](
	ctx context.Context,
	queueUrl string,
	handler MessageHandler[T],
	errDecorator func(ctx context.Context, errChan <-chan error),
) {

	cfg := sqsConfig.NewConfig(queueUrl, *aws.GetConfig())

	consumer := sqsConfig.NewSQSMessageConsumerFromConfig(cfg)
	go errDecorator(ctx, consumer.Errors())

	msgHandler := buildMessageHandler(handler)
	processor := messaging.NewChannelMessageProcessor(1, msgHandler)
	go errDecorator(ctx, processor.Errors())

	log.Println(ctx, fmt.Sprintf("subscribe on queue: %q", queueUrl))
	messaging.RunWithSignal(ctx, consumer, processor)
}

func handlerErrChan(ctx context.Context, errChan <-chan error) {
	logC := log.New(os.Stdout, "Consumer Error: ", 0)
	for err := range errChan {
		log.Println(ctx, err.Error())
		logC.Println(err)
	}
}

func buildMessageHandler[T any](handler MessageHandler[T]) messaging.MessageHandler {
	jsonDeserializer := jsonDeserializer[T]{}
	return func(ctx context.Context, msg messaging.MessageWrapper) error {
		m := Message[T]{
			BodyString:   msg.Body(),
			deserializer: jsonDeserializer,
		}

		messageHandler := handler(ctx, m)
		return messageHandler
	}
}

func getQueueUrl(ctx context.Context, queueName string) string {
	input := &sqs.GetQueueUrlInput{
		QueueName: &queueName,
	}
	urlResult, err := sqsClient().GetQueueUrl(ctx, input)
	if err != nil {

		log.Println(ctx, "error getting queue url", err)
		return ""
	}
	return *urlResult.QueueUrl
}

func sqsClient() *sqs.Client {
	sqsOnce.Do(func() {
		sqsInstance = sqs.NewFromConfig(*config.GetConfig())
	})
	return sqsInstance
}
