package consumer

// Message represents the received message from SQS (this is not a raw message)
type Message[T any] struct {
	BodyString   string
	deserializer deserializer[T]
}

/*
ReadBody deserialize the content string to the given type
*/
func (m Message[T]) ReadBody() (T, error) {
	return m.deserializer.Deserialize(m.BodyString)
}
