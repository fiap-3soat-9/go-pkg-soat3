package consumer

import "encoding/json"

type deserializer[T any] interface {
	Deserialize(input string) (T, error)
}

type jsonDeserializer[T any] struct {
}

func (jsonDeserializer[T]) Deserialize(input string) (T, error) {
	var output T
	err := json.Unmarshal([]byte(input), &output)

	return output, err
}
