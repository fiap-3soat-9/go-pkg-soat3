package consumer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestJsonDeserializer(t *testing.T) {
	type Product struct {
		Name string `json:"name"`
	}

	t.Run("Deserialize success", func(t *testing.T) {

		stringContent := `{"name": "product name"}`
		deserializer := jsonDeserializer[Product]{}
		p, err := deserializer.Deserialize(stringContent)

		assert.NoError(t, err)
		assert.NotNil(t, p)
		assert.Equal(t, "product name", p.Name)
	})

	t.Run("Deserialize failure", func(t *testing.T) {

		stringContent := `{"name: "product name"}`
		deserializer := jsonDeserializer[*Product]{}
		p, err := deserializer.Deserialize(stringContent)

		assert.Error(t, err)
		assert.Nil(t, p)
	})
}
