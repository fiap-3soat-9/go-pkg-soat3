package consumer

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

type dummyDeserializer[T any] struct {
	err    error
	result T
}

func (d dummyDeserializer[T]) Deserialize(string) (T, error) {
	return d.result, d.err
}

func TestMessage(t *testing.T) {
	type Product struct {
		Name string
	}

	t.Run("ReadBody should return deserializer result", func(t *testing.T) {
		d := dummyDeserializer[Product]{
			err:    nil,
			result: Product{Name: "test"},
		}
		message := Message[Product]{
			deserializer: d,
		}

		p, err := message.ReadBody()

		assert.NoError(t, err)
		assert.Equal(t, d.result, p)
	})

	t.Run("ReadBody should return deserializer error", func(t *testing.T) {
		d := dummyDeserializer[*Product]{
			err:    errors.New("error"),
			result: nil,
		}
		message := Message[*Product]{
			deserializer: d,
		}

		p, err := message.ReadBody()

		assert.Error(t, err)
		assert.Nil(t, p)
	})
}
