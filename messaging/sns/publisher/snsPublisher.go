package publisher

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	aws2 "gitlab.com/fiap-3soat-9/go-pkg-soat3/aws"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
	application "gitlab.com/fiap-3soat-9/go-pkg-soat3/starter"
	"sync"

	"github.com/aws/aws-sdk-go-v2/service/sns"
	"github.com/aws/aws-sdk-go-v2/service/sns/types"
)

var (
	snsInstance *sns.Client
	snsOnce     sync.Once
)

type SnsPublisher struct {
	ctx          context.Context
	messageInput *sns.PublishInput
	batchInput   *[]types.PublishBatchRequestEntry
	Payload      message.Serializer
	Topic        string
	Arn          string
	Attrs        map[string]types.MessageAttributeValue
}

/*
Publish send the event to topic
*/
func (p *SnsPublisher) Publish() error {
	serializedMessage, err := p.Payload.Serialize()
	if err != nil {
		println("[Publish Failed] failed to serialize", err)
		return err
	}

	p.messageInput = &sns.PublishInput{
		Message:           serializedMessage,
		MessageAttributes: p.Attrs,
		TopicArn:          &p.Arn,
	}

	_, err = snsClient().Publish(p.ctx, p.messageInput)
	if err != nil {
		println("[Publish Failed]", err)
		return err
	}
	return nil
}

/*
WithMessageAttributes sets attributes to event
returns the instance of SnsPublisher
*/
func (p *SnsPublisher) WithMessageAttributes(attrs map[string]struct{ Type, Value string }) *SnsPublisher {
	for k, v := range attrs {
		p.Attrs[k] = types.MessageAttributeValue{
			DataType:    aws.String(v.Type),
			StringValue: aws.String(v.Value),
		}
	}
	return p
}

/*
NewSNSPublisher creates a new publisher informing topic name and message with this respective serializer
returns a new instance of SnsPublisher
*/
func NewSNSPublisher(ctx context.Context, topicName string, msg message.Serializer) *SnsPublisher {
	publisher := &SnsPublisher{
		ctx:     ctx,
		Payload: msg,
		Topic:   topicName,
		Arn:     arnFormat(topicName),
		Attrs:   make(map[string]types.MessageAttributeValue),
	}

	return publisher
}

func arnFormat(topicName string) string {
	snsArnBase := fmt.Sprintf(
		"arn:aws:sns:%s:%s",
		application.GetAwsConfig().Region,
		application.GetAwsConfig().AccountId,
	)
	return fmt.Sprintf("%s:%s", snsArnBase, topicName)
}

func snsClient() *sns.Client {
	snsOnce.Do(func() {
		snsInstance = sns.NewFromConfig(*aws2.GetConfig())
	})
	return snsInstance
}
