`auto generated documentation`

## Usage

#### type SnsPublisher

```go
type SnsPublisher struct {
	Payload message.Serializer
	Topic   string
	Arn     string
	Attrs   map[string]types.MessageAttributeValue
}
```


#### func  NewSNSPublisher

```go
func NewSNSPublisher(ctx context.Context, topicName string, msg message.Serializer) *SnsPublisher
```
NewSNSPublisher creates a new publisher informing topic name and message with
this respective serializer returns a new instance of SnsPublisher

#### func (*SnsPublisher) Publish

```go
func (p *SnsPublisher) Publish() error
```
Publish send the event to topic

#### func (*SnsPublisher) WithMessageAttributes

```go
func (p *SnsPublisher) WithMessageAttributes(attrs map[string]struct{ Type, Value string }) *SnsPublisher
```
WithMessageAttributes sets attributes to event returns the instance of
SnsPublisher
