package publisher

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
	application "gitlab.com/fiap-3soat-9/go-pkg-soat3/starter"
	"testing"
)

type testMessage struct {
	Name string
}

func TestSNSPublisherTestSuite(t *testing.T) {
	suite.Run(t, new(SNSPublisherTestSuite))
}

type SNSPublisherTestSuite struct {
	suite.Suite
	messagePayload testMessage
}

func (suite *SNSPublisherTestSuite) SetupSuite() {
	application.Initialize()
	suite.messagePayload = testMessage{Name: "test rafael"}
}

func (suite *SNSPublisherTestSuite) Test_arnFormat() {
	publisher := NewSNSPublisher(context.TODO(), "TEST_TOPIC", message.JsonSerializer[testMessage]{Payload: suite.messagePayload})
	expected := "arn:aws:sns:::TEST_TOPIC"
	suite.T().Log(publisher.Arn)
	assert.Equal(suite.T(), expected, publisher.Arn)
}

func (suite *SNSPublisherTestSuite) Test_bindMessageAttributes() {
	publisher := NewSNSPublisher(context.TODO(), "TEST_TOPIC", message.JsonSerializer[testMessage]{Payload: suite.messagePayload}).
		WithMessageAttributes(map[string]struct{ Type, Value string }{
			"event_type": {Type: "String", Value: "CARDS_CREATED"},
		})

	expected := "CARDS_CREATED"
	assert.Equal(suite.T(), &expected, publisher.Attrs["event_type"].StringValue)
}

func (suite *SNSPublisherTestSuite) Test_snsPublisher_WithEvent() {
	messageTemplate := *message.NewMessageWrapper[testMessage]().
		WithBody(suite.messagePayload).
		WithTopicEvent("EVENT_TEST").
		WithTopic("TEST_TOPIC")

	publisher := NewSNSPublisher(context.TODO(), "TEST_TOPIC", message.JsonSerializer[message.Message[testMessage]]{Payload: messageTemplate})
	serializeMessage, err := publisher.Payload.
		assert.NotNil(suite.T(), serializeMessage)
	assert.Nil(suite.T(), err)
	assert.NotNil(suite.T(), publisher.Arn)
	assert.NotNil(suite.T(), publisher.Topic)
}
