package messaging

import (
	"context"
	"encoding/json"
	"errors"

	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/util"
)

// A MessageProcessor reads messages from a channel and processes them.
type MessageProcessor interface {
	// Run reads messages from channel and processes them until ctx is cancelled. This is blocking.
	Run(ctx context.Context, channel <-chan Message) error

	// Errors returns a channel that receives errors that ocurred during message processing.
	// This channel will be closed after Run returns.
	Errors() <-chan error
}

// MessageWrapper satisfies the model.Message interface while providing additional useful functions.
type MessageWrapper struct {
	msg Message
}

func (wrapper MessageWrapper) Body() string {
	return wrapper.msg.Body()
}

func (wrapper MessageWrapper) MarkAsRead(ctx context.Context) error {
	return wrapper.msg.MarkAsRead(ctx)
}

var _ Message = MessageWrapper{}

// Wraps a message into a MessageWrapper
func Wrap(msg Message) MessageWrapper {
	return MessageWrapper{msg}
}

// BindJSON parses and validates the body of a Message and binds the result to a target struct.
//
// See encoding/json and github.com/go-playground/validator for reference on the annotations for target struct types.
func (wrapper MessageWrapper) BindJSON(target interface{}) error {
	if err := json.Unmarshal([]byte(wrapper.msg.Body()), &target); err != nil {
		return err
	}
	if err := Validator.Struct(target); err != nil {
		return err
	}
	return nil
}

type MessageHandler = func(ctx context.Context, msg MessageWrapper) error

type channelMessageProcessor struct {
	parallelism int
	handler     MessageHandler
	errors      chan error
}

// NewChannelMessageProcessor creates a message processor that listens to messages from channel with a number of
// goroutines equal to parallelism.
//
// The processor will recover from panics raised by handler and continue processing.
func NewChannelMessageProcessor(
	parallelism int,
	handler MessageHandler,
) MessageProcessor {
	errors := make(chan error)
	return &channelMessageProcessor{parallelism, handler, errors}
}

func (p *channelMessageProcessor) Errors() <-chan error {
	return p.errors
}

func (p *channelMessageProcessor) Run(ctx context.Context, channel <-chan Message) error {
	defer close(p.errors)

	g := util.NewWorkerGroup()
	g.Spawn(p.parallelism, func() {
		for {
			select {
			case <-ctx.Done():
				return
			case message := <-channel:
				var err error
				func() {
					defer func() {
						if r := recover(); r != nil {
							err = errors.New("panicked while handling message")
						}
					}()
					err = p.handler(ctx, Wrap(message))
					if err == nil {
						err = message.MarkAsRead(context.Background())
					}
				}()
				if err != nil {
					p.sendError(err)
				}
			}
		}
	})
	g.Wait()

	return nil
}

func (p *channelMessageProcessor) sendError(err error) {
	select {
	case p.errors <- err:
	default:
	}
}
