package message

const payloadVersion = "2019-07-09"

// Message Wrapper to messages like by hamburgueria messaging template
type Message[T any] struct {
	Id        string  `json:"id,omitempty"`
	Body      Body[T] `json:"body" validate:"required"` //Content message
	BodyClass string  `json:"bodyClass,omitempty"`
}

type Body[T any] struct {
	Topic    string `json:"topic,omitempty"` //Name of the Topic to produce message
	Name     string `json:"name,omitempty"`  //Event name
	Contents T      `json:"contents" validate:"required"`
	Version  string `json:"version,omitempty"`
	Source   string `json:"source,omitempty"` //Message origin, ex: my application name
}

/*
NewMessageWrapper creates a Message following the template by hamburgueria messaging lib
*/
func NewMessageWrapper[T any]() *Message[T] {
	return &Message[T]{
		Body: Body[T]{
			Version: payloadVersion,
		},
	}
}

/*
WithBody provide contents to body
*/
func (m *Message[T]) WithBody(body T) *Message[T] {
	m.Body.Contents = body
	return m
}

func (m *Message[T]) WithTopic(topic string) *Message[T] {
	m.Body.Topic = topic
	return m
}

func (m *Message[T]) WithTopicEvent(event string) *Message[T] {
	m.Body.Name = event
	return m
}

func (m *Message[T]) WithSource(source string) *Message[T] {
	m.Body.Source = source
	return m
}
