package message

import (
	"encoding/json"
)

// Serializer interface needed to produce messages
type Serializer interface {
	Serialize() (*string, error)
}

// JsonSerializer provide a Serializer of payload to JSON
type JsonSerializer[T any] struct {
	Payload T
}

func (m JsonSerializer[T]) Serialize() (*string, error) {
	return toJSONString(m.Payload)
}

func toJSONString(source any) (*string, error) {
	output, err := json.Marshal(source)
	if err != nil {
		return nil, err
	}
	stringOutput := string(output)
	return &stringOutput, nil
}
