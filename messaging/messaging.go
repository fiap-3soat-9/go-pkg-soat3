package messaging

import (
	"context"
	"os"
	"os/signal"
	"syscall"
)

// Run joins a consumer and a processor and runs them concurrently until ctx is canceled.
func Run(ctx context.Context, consumer MessageConsumer, processor MessageProcessor) error {
	messages := make(chan Message)
	go consumer.Run(ctx, messages)
	return processor.Run(ctx, messages)
}

// RunWithSignal joins a consumer and a processor and runs them concurrently until the processes receives either a
// SIGINT or a SIGTERM. Repeated signals will cause a panic.
func RunWithSignal(ctx context.Context, consumer MessageConsumer, processor MessageProcessor) error {
	ctx, cancel := context.WithCancel(context.Background())

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)

	defer func() {
		signal.Stop(sig)
		cancel()
	}()

	go func() {
		select {
		case <-sig:
			cancel()
		case <-ctx.Done():
		}
		<-sig
		panic("repeated interruption signal")
	}()

	return Run(ctx, consumer, processor)
}
