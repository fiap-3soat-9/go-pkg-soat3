package messaging

import "context"

type Message interface {
	Body() string
	MarkAsRead(ctx context.Context) error
}
