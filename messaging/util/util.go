package util

import "sync"

type Task = func()

type WorkerGroup interface {
	Spawn(n int, task Task)
	Wait()
}

type workerGroup struct {
	w sync.WaitGroup
}

func NewWorkerGroup() WorkerGroup {
	return &workerGroup{}
}

func (g *workerGroup) Spawn(n int, task Task) {
	g.w.Add(n)
	for i := 0; i < n; i++ {
		go func() {
			defer g.w.Done()
			task()
		}()
	}
}

func (g *workerGroup) Wait() {
	g.w.Wait()
}
