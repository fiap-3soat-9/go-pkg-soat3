package messaging

import "github.com/go-playground/validator/v10"

// The validator used to check structs parsed by BindJSON.
var Validator *validator.Validate = validator.New()

func init() {
	Validator.SetTagName("binding") // Same as gin-gonic, for compatibility
}
