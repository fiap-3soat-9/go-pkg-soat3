package messaging

import (
	"context"
)

// A MessageConsumer reads messages from some source and sends them into a channel.
type MessageConsumer interface {
	// Run reads messages and send them to channel until ctx is cancelled. This is blocking.
	Run(ctx context.Context, channel chan<- Message) error

	// Errors returns a channel that receives errors that ocurred during message processing.
	// This channel will be closed after Run returns.
	Errors() <-chan error
}
